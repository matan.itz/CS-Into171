import java.util.Comparator;

/**
 * Created by shalev on 10/01/2017.
 */
public class EntryComparatorByNumberSpec {
    public static void main(String[] args) {
        EntryComparatorByNumber ec = new EntryComparatorByNumber();
        PhoneEntry pe = new PhoneEntry("name", 123);
        System.out.println("Same object");
        validateCompare(ec, pe, pe);
        System.out.println("Same number");
        validateCompare(ec, pe, new PhoneEntry("other name", 123));
        System.out.println("with 122");
        validateCompare(ec, pe, new PhoneEntry("name", 122));
        System.out.println("with 124");
        validateCompare(ec, pe, new PhoneEntry("name", 124));

        try {
            System.out.print("try with other element: ");
            validateCompare(ec, pe, 123);
            System.out.println("No Exception");
        } catch (Exception ex) {
            System.out.println(ex.getClass());
        }
    }

    public static void validateCompare(Comparator comp, Object obj1, Object obj2) {
        System.out.println("compare(obj1,obj2): " + (int) (Math.signum(comp.compare(obj1, obj2)))); // negative to -1, positive to 1
        System.out.println("compare(obj2,obj1): " + (int) (Math.signum(comp.compare(obj2, obj1))));
        System.out.println("compare(obj1,obj1): " + comp.compare(obj1, obj1));
    }
}
