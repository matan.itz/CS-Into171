/**
 * Created by shalev on 10/01/2017.
 */
public class BinaryTreeInOrderIteratorSpec {
    public static void main(String[] args) {
        BinaryNode bn = new BinaryNode(2);
        bn.left = new BinaryNode(1);
        bn.right = new BinaryNode(3);
        BinaryTreeInOrderIterator iterator = new BinaryTreeInOrderIterator(bn);

        // hasNext & next
        System.out.println("hasNext(): " + iterator.hasNext());
        System.out.println("next(): 1 -> " + iterator.next());
        System.out.println("next(): 2 -> " + iterator.next());
        System.out.println("next(): 3 -> " + iterator.next());
        System.out.println("hasNext(): " + iterator.hasNext());
        try {
            System.out.print("try next() with nothing left: ");
            iterator.next();
            System.out.println("No Exception");
        } catch (Exception ex){
            System.out.println(ex.getClass());
        }
    }
}
