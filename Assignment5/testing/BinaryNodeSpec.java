/**
 * Created by shalev on 13/01/2017.
 */
public class BinaryNodeSpec {
    public static void main(String[] args) {
        BinaryNode root = new BinaryNode(1);
        printWithNL("only root: " + root.toString());
        BinaryNode left = new BinaryNode(2);
        root.left = left;
        printWithNL("root&left: " + root.toString());
        BinaryNode right = new BinaryNode(3);
        root.right = right;
        printWithNL("root&left-right: " + root.toString());
        BinaryNode otherLeft = new BinaryNode(4);
        left.left = otherLeft;
        printWithNL("root&left-right&left: " + root.toString());
        BinaryNode otherLeft2 = new BinaryNode(5);
        otherLeft.left = otherLeft2;
        printWithNL("root&left-right&left&left: " + root.toString());
        BinaryNode otherRight = new BinaryNode(6);
        otherLeft.right = otherRight;
        printWithNL("root&left-right&right&left&left: " + root.toString());
    }

    public static void printWithNL(String str) {
        System.out.println(str.replace(System.getProperty("line.separator"), "\\n"));
    }
}
