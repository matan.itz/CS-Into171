import java.util.Comparator;

/**
 * Created by shalev on 10/01/2017.
 */
public class EntryComparatorByNameSpec {
    public static void main(String[] args) {
        EntryComparatorByName ec = new EntryComparatorByName();
        PhoneEntry pe = new PhoneEntry("name", 123);
        System.out.println("Same object");
        validateCompare(ec, pe, pe);
        System.out.println("Same name");
        validateCompare(ec, pe, new PhoneEntry("name", 345));
        System.out.println("with name1");
        validateCompare(ec, pe, new PhoneEntry("name1", 123));
        System.out.println("with namf");
        validateCompare(ec, pe, new PhoneEntry("namf", 123));
        System.out.println("with namd");
        validateCompare(ec, pe, new PhoneEntry("namd", 123));

        try {
            System.out.print("try with other element: ");
            validateCompare(ec, pe, new String("name"));
            System.out.println("No Exception");
        } catch (Exception ex) {
            System.out.println(ex.getClass());
        }
    }

    public static void validateCompare(Comparator comp, Object obj1, Object obj2) {
        System.out.println("compare(obj1,obj2): " + (int) (Math.signum(comp.compare(obj1, obj2)))); // negative to -1, positive to 1
        System.out.println("compare(obj2,obj1): " + (int) (Math.signum(comp.compare(obj2, obj1))));
        System.out.println("compare(obj1,obj1): " + comp.compare(obj1, obj1));
    }
}
