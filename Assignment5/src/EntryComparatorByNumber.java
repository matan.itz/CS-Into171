import java.util.Comparator;

public class EntryComparatorByNumber implements Comparator{

	public int compare(Object o1, Object o2) {
		//checking that the objects are of "PhoneEntry" type:
		if(!(o1 instanceof PhoneEntry) || !(o2 instanceof PhoneEntry)) {
			throw new ClassCastException("Objects must be instance of PhoneEntry");
		}
		PhoneEntry entry1=(PhoneEntry)o1;
		PhoneEntry entry2=(PhoneEntry)o2;
		//extracting the phone numbers:
		Integer number1=entry1.getNumber();
		Integer number2=entry2.getNumber();
		//returning the comparison result:
		return number1.compareTo(number2);
	}

}
