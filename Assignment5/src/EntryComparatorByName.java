import java.util.Comparator;

public class EntryComparatorByName implements Comparator{

	public int compare(Object o1, Object o2) {
		//checking that the objects are of "PhoneEntry" type:
		if(!(o1 instanceof PhoneEntry) || !(o2 instanceof PhoneEntry)) { 
			throw new ClassCastException("Objects must be instance of PhoneEntry");
		}
		PhoneEntry entry1=(PhoneEntry)o1;
		PhoneEntry entry2=(PhoneEntry)o2;
		//extracting the names:
		String name1=entry1.getName();
		String name2=entry2.getName();
		//returning the comparison result:
		return name1.compareTo(name2);
	}

}
