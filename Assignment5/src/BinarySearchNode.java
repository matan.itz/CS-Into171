import java.util.Comparator;
import java.util.Iterator;

public class BinarySearchNode extends BinaryNode{

	private Comparator treeComparator;
	
	public BinarySearchNode(Object data, Comparator myComparator) {
		super(data);
		this.treeComparator = myComparator;
	}
	
	// assume otherTreeRoot.size()>0
	public BinarySearchNode(BinarySearchNode otherTreeRoot, Iterator otherTreeIterator) {
		super("dummy");
		treeComparator = otherTreeRoot.getComparator();
		buildPerfectTree(otherTreeRoot.size());
		fillTheNodes(this, otherTreeIterator);
	}
	
	// element is an Entry with one "dummy" field
	public Object findData(Object element){
		if (treeComparator.compare(data, element) > 0){
			if (left != null )
				return ((BinarySearchNode)left).findData(element);
			else
				return null;
		}
		else if(treeComparator.compare(data, element) < 0){
			if (right != null )
				return ((BinarySearchNode)right).findData(element);
			else
				return null;
		}
		else 
			return this.data;
	}
	
	public Object findMin(){
		BinaryNode currNode = this;
		while (currNode.left != null){
			currNode = currNode.left;
		}
		return currNode.data;
	}
	
	// Complete the following methods:
	
	private void buildPerfectTree(int size){
		Queue q = new QueueAsLinkedList();
		q.enqueue(this);
		// create the remaining size-1 nodes in the tree
		/* Enter you code here:*/
		int n=1; //number of nodes at the moment
		while(n<size) { //as long as the number of nodes at the moment is less than the number that's supposed to be
			Object nodeFromQueue=q.dequeue(); //getting a node from the queue
			if(nodeFromQueue instanceof BinarySearchNode) {
				BinarySearchNode nextNode=(BinarySearchNode)nodeFromQueue;
				BinarySearchNode leftDummyNode=new BinarySearchNode("dummy",this.getComparator()); //creating a new node
				nextNode.left=leftDummyNode; //adding the new node to the left of the node-from-queue
				q.enqueue(leftDummyNode); //adding the new-left-node to the queue
				n++; //increasing the size of available nodes
				if(n<size) { //we haven't reached the number of nodes yet
					BinarySearchNode rightDummyNode=new BinarySearchNode("dummy",this.getComparator()); //creating a new node
					nextNode.right=rightDummyNode; //adding the new node to the right of the node-from-queue
					q.enqueue(rightDummyNode); //adding the new-right-node to the queue
					n++; //increasing the size of available nodes
				}
			}
		}
		// now this is the root of a tree with size dummy nodes
	}
	
	private void fillTheNodes(BinarySearchNode root, Iterator treeIterator){
		//going over the binary tree according to the in-order traversal (left-middle-right)
		if(root.left != null) {
			fillTheNodes((BinarySearchNode)root.left, treeIterator);
		}
		root.data=treeIterator.next(); //replacing the "dummy" data with data from the iterator
		if(root.right != null) {
			fillTheNodes((BinarySearchNode)root.right, treeIterator);
		}
	}

	public Comparator getComparator(){
		return this.treeComparator;
	}

	public void insert(Object toInsert) {
		if(toInsert==null) { //wrong input - null to insert is not possible
			throw new NullPointerException("object to insert cannot be null");
		}
		if(this.contains(toInsert)) { //wrong input - object is already in the binary tree 
			return;
		}
		Comparator myComp=this.getComparator();
		BinarySearchNode nodeToAdd=new BinarySearchNode(toInsert, this.getComparator()); //creating a new node with the given object
		if(myComp.compare(this.data, nodeToAdd.data)<0) { //new object should be added to the right
			if(right==null) { //right place is empty - safe to add in that place
				this.right=nodeToAdd;
			}
			else { //needs to go to a lower level
				BinarySearchNode rightNode=(BinarySearchNode)right;
				rightNode.insert(toInsert); //trying to insert the new node to the right again, in a lower level
			}
		}
		if(myComp.compare(this.data, nodeToAdd.data)>0) { //new object should be added to the left
			if(left==null) { //left place is empty - safe to add in that place
				this.left=nodeToAdd;
			}
			else { //needs to go to a lower level
				BinarySearchNode leftNode=(BinarySearchNode)left;
				leftNode.insert(toInsert); //trying to insert the new node to the left again, in a lower level
			}
		}
	}
	
	public boolean contains(Object element) {
		if(element==null) { //wrong input - null object is not possible
			return false;
		}
		boolean isNodeExists=false;
		BinaryNode checkNode=this; //beginning the check with the root
		Comparator myComp=this.getComparator();
		while(checkNode!=null && !isNodeExists) { //go over the binary tree as long as wanted element not found and as far as you can go
			if(myComp.compare(checkNode.data, element)==0) { //this is the desired node
				isNodeExists=true;
			}
			else if(myComp.compare(checkNode.data, element)<0) { //node is supposed to be on the right
				checkNode=checkNode.right;
			}
			else { //node is supposed to be on the left
				checkNode=checkNode.left;
			}
		}
		return isNodeExists;
	}
	
	public BinaryNode remove(Object toRemove){
		if(toRemove==null) { //wrong input - null object is not possible
			throw new NullPointerException("object to remove cannot be null");
		}
		if(!this.contains(toRemove)) { //wrong input - the object does not exists - meaning we can't remove it
			return this;
		}
		BinaryNode nodeToRemove=findWantedNode(toRemove); //finding the node with that object
		BinaryNode parentNode=getParent(this, nodeToRemove); //getting the parent of that node
		return applyRemovale(nodeToRemove, parentNode); //applying the removable process
	}
	
	private BinaryNode findWantedNode(Object wantedObject) {
		Comparator myComp=this.getComparator();
		if(myComp.compare(this.data, wantedObject)==0){ //this is the node to remove
			return this;
		}
		if(myComp.compare(this.data, wantedObject)<0) { //node is supposed to be on the right
			return ((BinarySearchNode)right).findWantedNode(wantedObject); //keep searching, to your right
		}
		if(myComp.compare(this.data, wantedObject)>0) { //node is supposed to be on the left
			return ((BinarySearchNode)left).findWantedNode(wantedObject); //keep searching, to your left
		}
		return this;
	}
	

	private BinaryNode getParent(BinaryNode parent, BinaryNode wantedNode) {
		Comparator myComp=this.getComparator();
		if(parent==null || wantedNode==null || parent.equals(wantedNode)) { //there is not parent (maybe because this is the root)
			return null;
		}
		if((parent.right!=null && parent.right.data.equals(wantedNode.data)) || (parent.left!=null && parent.left.data.equals(wantedNode.data))) {
			return parent; //the wanted node appears to the right or to the left of this node - meaning this is the parent node
		}
		else {
			if(myComp.compare(parent.data, wantedNode.data)<0) { //supposed to be on the right
				return getParent(parent.right, wantedNode); //keep searching, to your right
			}
			if(myComp.compare(parent.data, wantedNode.data)>0) { //supposed to be on the left
				return getParent(parent.left, wantedNode); //keep searching, to your left
			}
		}
		return parent;
	}
	
	private BinaryNode applyRemovale(BinaryNode removeNode, BinaryNode parentNode) {
		if(removeNode.left==null && removeNode.right==null) { //no sons
			return setReplacement(parentNode, removeNode, null); //sets corresponding link of the parent to NULL and disposes the node
		}
		if(removeNode.left==null) { //one son only
			return setReplacement(parentNode, removeNode, removeNode.right); //set the single child (with it's subtree) directly to the parent of the removed node
		}
		if(removeNode.right==null) { //one son only
			return setReplacement(parentNode, removeNode, removeNode.left); //set the single child (with it's subtree) directly to the parent of the removed node
		}
		if(removeNode.left!=null && removeNode.right!=null) { //two sons
			//find a minimum value in the right subtree
			BinaryNode minNode = removeNode.right;
			BinaryNode minNodeParent=removeNode;
			while (minNode.left != null){
				minNodeParent=minNode;
				minNode = minNode.left;
			}
			//replace value of the node to be removed with found minimum
			removeNode.data=minNode.data;
			//apply remove to the right subtree to remove a duplicate
			applyRemovale(minNode, minNodeParent);
			return this;
		}
		return parentNode;
	}
	
	private BinaryNode setReplacement(BinaryNode parentNode, BinaryNode removeNode,BinaryNode replacementNode) {
		Comparator myComp=this.getComparator();
		if(parentNode==null) {
			removeNode=replacementNode;
			return removeNode;
		}
		if(myComp.compare(parentNode.data, removeNode.data)<0) {//supposed to be on the right
			parentNode.right=replacementNode;
			return parentNode;
		}
		if(myComp.compare(parentNode.data, removeNode.data)>0) { //supposed to be on the left
			parentNode.left=replacementNode;
			return parentNode;
		}
		if(myComp.compare(parentNode.data, removeNode.data)==0) { //duplicate
			if(parentNode.left.data.equals(parentNode.data)) {
				parentNode.left=null;
			}
			if(parentNode.right.data.equals(parentNode.data)) {
				parentNode.right=null;
			}
		}
		return parentNode;
	}
}


