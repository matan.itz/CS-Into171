public class Phonebook {

	private BinarySearchTree namesTree;
	private BinarySearchTree numbersTree;
	
	public Phonebook() {
		namesTree = new BinarySearchTree(new EntryComparatorByName());
		numbersTree = new BinarySearchTree(new EntryComparatorByNumber());
	}

	public PhoneEntry lookUp(String name){
		// create an Entry with the given name and a "dummy" number (1)
		// This "dummy" number will be ignored when executing getData
		PhoneEntry lookFor = new PhoneEntry(name, 1);
		return (PhoneEntry)namesTree.findData(lookFor);
	}
	public PhoneEntry lookUp(int number){
		// create an Entry with a "dummy" name and the given number
		// This "dummy" name will be ignored when executing getData
		PhoneEntry lookFor = new PhoneEntry("dummy", number);
		return (PhoneEntry)numbersTree.findData(lookFor);
	}
	
	public void balance(){
		namesTree = new BinarySearchTree(namesTree);
		numbersTree = new BinarySearchTree(numbersTree);
	}
	
	public Object exportNames() {
		return this.namesTree;
	}
	public Object exportNumbers() {
		return this.numbersTree;
	}
	
	// Complete the following methods:
	
	public boolean add(PhoneEntry newEntry) {
		if(namesTree.contains(newEntry)|| numbersTree.contains(newEntry)) { //phone entry already exists - couldn't add it again
			return false;
		}
		//adding the phone entry to both binary search trees:
		namesTree.insert(newEntry);
		numbersTree.insert(newEntry);
		return true;
	}
	
	public boolean delete(String name){
		PhoneEntry toRemove = lookUp(name);
		/* Enter you code here:*/
		if(toRemove!=null && namesTree.contains(toRemove)) { //there's such object and it exists within the namesTree
			//deleting the phone entry from both binary search trees:
			namesTree.remove(toRemove);
			numbersTree.remove(toRemove);
			return true;
		}
		return false;
	}
	
	public boolean delete(int number){
		PhoneEntry toRemove = lookUp(number);
		/* Enter you code here:*/
		if(toRemove!=null && numbersTree.contains(toRemove)) { //there's such object and it exists within the numbersTree
			//deleting the phone entry from both binary search trees:
			namesTree.remove(toRemove);
			numbersTree.remove(toRemove);
			return true;
		}
		return false;
	}
	
	


}
