
public class StudentGradeAverageComparator implements Comparator {

	@Override
	public int compare(Object obj1, Object obj2) {
		if(!(obj1 instanceof Student) || !(obj2 instanceof Student)) { //the object is not an instance of Student - cannot compare it
			throw new ClassCastException("Objects must be instance of Student");
		}
        //casting the objects into 'Student':
		Student s1=(Student)obj1;
		Student s2=(Student)obj2;
        //calculating the average:
		double s1Avg=s1.calculateAverage();
		double s2Avg=s2.calculateAverage();
         /* Comparing method:
        Compares its two averages in descending order (highest average first, lowest average last)
        If the two averages are equal to each other - return zero
        If s1Avg is less than s2Avg - return 1 (so it will be placed last)
        If s1Avg is greater than s2Avg - return -1 (so it will be placed first)
        */
		if(s1Avg==s2Avg) {
			return 0;
		}
		else if (s1Avg<s2Avg) {
			return 1;
		}
		else{
			return -1;
		}
	}

}
