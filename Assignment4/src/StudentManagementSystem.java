import java.util.Iterator;

public class StudentManagementSystem {
	private LinkedList students;
	private LinkedList courses;
	private int numberOfStudents;
	private int numberOfCourses;
	
	public StudentManagementSystem() {
		this.students=new LinkedList();
		this.courses=new LinkedList();
		this.numberOfStudents=0;
		this.numberOfCourses=0;
	}
	
	public boolean addStudent(Student student){
		if(this.students.contains(student)) { //the student already in the system - return false
			return false;
		}
		this.students.add(student); //add the student to the system
		this.numberOfStudents++; //increase the number of students in the system
		return true;
	}
	
	public boolean addCourse(Course course){
		if(this.courses.contains(course)) { //the course already in the system - return false
			return false;
		}
		this.courses.add(course); //add the course to the system
		this.numberOfCourses++; //increase the number of courses in the system
		return true;
	}
	
	public boolean register(Student student, Course course){
		if(!this.students.contains(student) || !this.courses.contains(course)) { 
            //the student or the course is not registered in the system - return false
			return false;
		}
		return student.registerTo(course); //TODO: check if this affects the list too
	}
	
	public boolean addGradeToStudent(Student student, Course course, int grade){
		if(!this.students.contains(student) || !this.courses.contains(course)) {
            //the student or the course is not registered in the system - return false
			return false;
		}
		return student.addGrade(course, grade); //TODO: check if this affects the list too
	}
	
	public LinkedList getFirstKStudents(Comparator comp, int k){
		if(comp==null || this.numberOfStudents<k || k<0) { //wrong input
			throw new IllegalArgumentException("Can't compare with the given parameters");
		} 
		this.students.sortBy(comp); //sort the student list according to the comparator
		LinkedList sortedKStudents=new LinkedList(); //new list - only for the first k students
		int numberOfElements=0;
		Iterator studentIterator=this.students.iterator();
		while(studentIterator.hasNext() && numberOfElements<k) { //going over the list with iterator until we have k elements
			Student studentToAdd=(Student)studentIterator.next();
			sortedKStudents.add(studentToAdd);
			numberOfElements++;
		}
		return sortedKStudents;
	}
	
	public double computeFinalGrade(Student student){
		if(this.students.contains(student)) { //computing the final grade of the student
			return student.computeFinalGrade();
		}
		else{ //student is not registered in the system - return -1
			return -1;
		}
	}

	public int getNumberOfStudents() {
		return this.numberOfStudents;
	}

	public int getNumberOfCourses() {
		return this.numberOfCourses;
	}
}
