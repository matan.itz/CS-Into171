
public class EngineeringStudent extends Student {
	private static final int REQUIRED_CREDIT=160; //setting a final variable for the required credit

	public EngineeringStudent(String firstName, String lastName, int id) {
		super(firstName, lastName, id); //using the parent constructor
	}
	
	public int getTotalCreditRequired(){
		return REQUIRED_CREDIT; //returning the number of required credit
	}

}
