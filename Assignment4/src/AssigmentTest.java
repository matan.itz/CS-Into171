import java.util.Iterator;

public class AssigmentTest {

	public static void main(String[] args) {
		// ======= Tests for class Link =========
		
		System.out.println("Testing class Link");
		// getData
		Link link1 = new Link(new Integer(1));
		System.out.println("	Link getData " + link1.getData().equals(new Integer(1)));
		
		//getNext
		Link link2 = new Link(new Integer(2), link1);
		System.out.println("	Link: getNext " + (link2.getNext().equals(link1) && 
												   link1.getNext() == null));
		
		//setNext
		Link link3 = new Link(new Integer(0));
		link3.setNext(link2);
		link1.setNext(link3);
		System.out.println("	Link setNext " + (link3.getNext().equals(link2) && 
												  link1.getNext().equals(link3)));
		
		//setData
		link3.setData(new Integer(3));
		System.out.println("	Link: setData " + link3.getData().equals(new Integer(3)));
		
		//equals
		System.out.println("	Link: equals "+ link1.equals(new Link(new Integer(1))));
		System.out.println();
		
		
		// ======== Tests for class LinkedList ==========
		
		Integer i1 = new Integer(1);
		Integer i2 = new Integer(2);
		Integer i3= new Integer(3);
		
		System.out.println("Testing class LinkedList");
		
		//isEmpty
		System.out.println("	LinkedList: isEmpty "+ new LinkedList().isEmpty());
		
		//get
		LinkedList list1 = new LinkedList();
		list1.add(new Integer(1));
		list1.add(new Integer(2));
		list1.add(new Integer(3));
		try {
			list1.get(10);
			System.out.println("	LinkedList: get " + false);
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("	LinkedList: get " + list1.get(1).equals(new Integer(2)));
		}

		//size
		System.out.println("	LinkedList: size " + (list1.size() == 3));
		
		//contains
		System.out.println("	LinkedList: contains " + (!list1.contains(null) && 
														  list1.contains(new Integer(1))));
		
		//set
		try {
			list1.set(10, i2);
			System.out.println("	LinkedList: set " + false);
		}
		catch (IndexOutOfBoundsException e) {
			list1.set(1, new Integer(22));
			i2 = new Integer(22);
			System.out.println("	LinkedList: set " + list1.get(1).equals(i2));
		}
		
		//equals
		LinkedList list2 = new LinkedList();
		list2.add(i1);
		list2.add(i2);
		LinkedList list3 = new LinkedList();
		list3.add(i1);
		list3.add(new Integer(22));
		list3.add(i3);
		System.out.println("	LinkedList: equals " + (!list1.equals(list2) && 
														!list2.equals(list1) && 
														list1.equals(list3)));
		
		//soryBy
		
		//iterator
		LinkedList list4 = new LinkedList();
		int counter = 3;
		list4.add(i1);
		list4.add(i2);
		list4.add(i3);
		Iterator it = list4.iterator();
		boolean ans = true;
		while (it.hasNext() & ans) {
			counter--;
			ans = list4.contains(it.next());
		}
		System.out.println("	LinkedList: iterator " + (counter == 0 & ans));
		System.out.println();
		
		
		// ======== Tests for class Course ==========
		System.out.println("Testing class Course");
		Course c1 = new Course("intro", 202, 4);
		Course c2 = new Course("intro2" , 202, 100);
		
		//getCourseName
		System.out.println("	Course: getCourseName " + c1.getCourseName().equals("intro"));
		
		//getCourseNumber
		System.out.println("	Course: getCourseNumber " + (c1.getCourseNumber() == 202));
		
		//getCourseCredit
		System.out.println("	Course: getCourseCredit " + (c1.getCourseCredit() == 4));
		
		//equals
		System.out.println("	Course: equals " + (c1.equals(c2)));
		System.out.println();
		
		//======== Tests for class Grade ==========
		System.out.println("Testing class Grade");
		Grade g1 = new Grade(c1, 95);
		
		//getCourse
		System.out.println("	Grade: getCourse " + g1.getCourse().equals(c1));
		
		//getGrade
		System.out.println("	Grade: getGrade " + (g1.getGrade() == 95));
		
		//setGrade
		g1.setGrade(100);
		System.out.println("	Grade: setGrade " + (g1.getGrade() == 100));
		
		//equals
		Grade g2 = new Grade(c1, 100);
		Grade g3 = new Grade(new Course("SomeCourse",1,1),100);
		System.out.println("	Grade: equals " + (g1.equals(g2) && !g1.equals(g3)));
		System.out.println();
		
		//======== Tests for class Student ==========
		System.out.println("Testing class Student");
		Student s1 = new Student("Alice", "Bob",101);
		//getLastName
		System.out.println("	Student: getLastName " +s1.getLastName().equals("Bob"));
		
		//getFirstName
		System.out.println("	Student: getFirstName " + s1.getFirstName().equals("Alice"));
		
		//getId
		System.out.println("	Student: getId " + (s1.getId() == 101));
		
		//registerTo
		System.out.println("	Student: registerTo " + (s1.registerTo(c1) && 
														 !s1.registerTo(c1)));
		
		//addGrade
		c2 = new Course("B", 2, 5);
		System.out.println("	Student: addGrade " + (s1.addGrade(c1, 100) && 
													   !s1.addGrade(c2, 100)));
		
		//calculateAverage
		s1 = new Student("Alice", "Bob",101);
		c1 = new Course("A", 1, 1);
		
		Course c3 = new Course("C", 3, 4);
		s1.registerTo(c1);
		s1.registerTo(c2);
		s1.registerTo(c3);
		s1.addGrade(c1, 75);
		s1.addGrade(c2, 90);
		s1.addGrade(c3, 60);
		Student s2 = new Student("Daddy","Daddon", 777);
		System.out.println("	Student: calculateAverage " + (s1.calculateAverage() == 76.5 && 
															   s2.calculateAverage() == 0));
		
		//setGrade
		try {
			s2.setGrade(c1, 100);
			System.out.println("	Student: setGrade " + false);
		}
		catch(IllegalArgumentException e) {
			System.out.println("	Student: setGrade " + (s1.setGrade(c1, 100) == 75));
		}
		
		//equals
		System.out.println("	Student: equals " + (s1.equals(new Student("Gal", "David", 101)) &&  
													 !s1.equals(s2)));
		System.out.println();
		
		//======== Tests for class StudentNameComparator ==========
		Comparator nameComp = new StudentNameComparator();
		System.out.println("Testing class StudentNameComparator");
		Student s3 = new Student("Bob", "Bob", 999);
		System.out.println("	Test1: " + (nameComp.compare(s1, s2) < 0));
		System.out.println("	Test2: " + (nameComp.compare(s2, s1) > 0));
		try {
			nameComp.compare(s1, new Object());
		}
		catch (ClassCastException e) {
			System.out.println("	Test3: " + true);
		}
		System.out.println();
		
		//======== Tests for class StudentAverageComparator ==========
		Comparator avgComp = new StudentGradeAverageComparator();
		System.out.println("Testing class StudentAverageComparator");
		s3 = new Student("Bob", "Bob", 999);
		
		System.out.println("	Test1: " + (avgComp.compare(s1, s3) < 0));
		System.out.println("	Test2: " + (avgComp.compare(s3, s1) > 0));
		try {
			avgComp.compare(s1, new Object());
		}
		catch (ClassCastException e) {
			System.out.println("	Test3: " + true);
		}
		System.out.println();
		
		
		//======== Tests for class StudentManagementSystem ==========
		System.out.println("Testing class StudentManagementSystem");
		StudentManagementSystem sys = new StudentManagementSystem();
		s1 = new Student("Alice", "Bob", 101);
		s2 = new Student("Gal", "David", 1);
		c1 = new Course("intro", 202, 5);
		c2 = new Course("SomeCourse", 1, 1);
		
		//addStudent
		System.out.println("	Management: addStudent " + (sys.addStudent(s1) && 
															!sys.addStudent(s1)));
		
		//addCourse
		System.out.println("	Management: addCourse " + (sys.addCourse(c1) && 
														   !sys.addCourse(c1)));
		
		//register
		System.out.println("	Management: register " + (sys.register(s1, c1) && 
														  !sys.register(s1, c2) &&
														  !sys.register(s2, c1)));
		
		//getFirstKStudents
		s2 = new Student("Alice" ,"Alice", 102);
		s3 = new Student("Bob" ,"Bob", 103);
		Student s4 = new Student("Charlie" ,"Delta", 104);
		Student s5 = new Student("Yoni" ,"Zolo", 105);
		sys.addStudent(s3);
		sys.addStudent(s2);
		sys.addStudent(s5);
		sys.addStudent(s4);
		LinkedList res = sys.getFirstKStudents(new StudentNameComparator(), 3);
		LinkedList expected = new LinkedList();
		expected.add(s2);
		expected.add(s1);
		expected.add(s3);
		try {
			sys.getFirstKStudents(new StudentNameComparator(), 1000);
			System.out.println("	Management: getFirstKStudents " + false);
		}
		catch(IllegalArgumentException e) {
			System.out.println("	Management: getFirstKStudents " + expected.equals(res));
		}
		
		
		//addGradeToStudent
		s5.registerTo(c1);
		sys.addCourse(c2);
		s5.registerTo(c2);
		sys.addGradeToStudent(s5, c1, 100);
		sys.addGradeToStudent(s5, c2, 40);
		System.out.println("	Management: addGradeToStudent " + (s5.calculateAverage() == 90.0));
		System.out.println();
		
		
		//======== Tests for part 3 ==========
		System.out.println("Testing computeFinalGrade");
		
		//computerFinalGrade
		StudentManagementSystem sys2 = new StudentManagementSystem();
		s1 = new Student("Alice", "Bob", 101);
		c1 = new CsElectiveCourse("intro", 101, 80);
		c2 = new MathElectiveCourse("algebra", 202, 30);
		c3 = new Course("calculus", 303, 10);
		Course c4 = new ElectiveCourse("Economics", 505, 5);
		Course c5 = new CsElectiveCourse("AI", 453, 9); 
		
		sys2.addStudent(s1);
		sys2.addCourse(c1);
		sys2.addCourse(c2);
		sys2.addCourse(c3);
		sys2.register(s1, c1);
		sys2.register(s1, c2);
		sys2.register(s1, c3);
		sys2.addGradeToStudent(s1, c1, 100);
		sys2.addGradeToStudent(s1, c2, 70);
		sys2.addGradeToStudent(s1, c3, 80);
		
		//Course: computeFinalGrade
		System.out.println("	Course: computeFinalGrade " + (c1.computeFinalGrade(100) == 100 && 
																c1.computeFinalGrade(80) == 93 && 
																c3.computeFinalGrade(50) == 50 && 
																c2.computeFinalGrade(90) == 95 && 
																c4.computeFinalGrade(80) == 88 &&
																c5.computeFinalGrade(30) == 30));
		//Grade: computerFinalGrade
		g1 = new Grade(c1, 100);
		g2 = new Grade(c1,80);
		g3 = new Grade(c3,50);
		Grade g4 = new Grade(c2,90);
		Grade g5 = new Grade(c4,80);
		Grade g6 = new Grade(c5,30);
		System.out.println("	Grade: computeFinalGrade " + (g1.computeFinalGrade() == 100 && 
																g2.computeFinalGrade() == 93 && 
																g3.computeFinalGrade() == 50 && 
																g4.computeFinalGrade() == 95 && 
																g5.computeFinalGrade() == 88 &&
																g6.computeFinalGrade() == 30));
		
		//Student computeFinalGrade
		System.out.println("	Student: computeFinalGrade " + ((int)s1.computeFinalGrade() == 92 && 
																s2.computeFinalGrade() == -1));
		
		
		//StudentManagementSystem computeFinalGrade
		System.out.println("	Management: computeFinalGrade " + ((int)sys2.computeFinalGrade(s1) == 92 &&
																	sys2.computeFinalGrade(s2) == -1));
		//FinalGradeComparator
		Comparator gradeComp = new FinalGradeComparator();
		try {
			gradeComp.compare(g1, s2);
			System.out.println("	FinalGradeComparator " + false);
		}catch(ClassCastException e) {
			System.out.println("	FinalGradeComparator " + (gradeComp.compare(g1, g3) < 0 && 
															  gradeComp.compare(g3, g5) > 0 && 
															  gradeComp.compare(g1, g1) == 0));
		}
	
		
		//getTotalCreditRequired
		s2 = new EngineeringStudent("Elon", "Musk", 111);
		System.out.println("	getTotalCreditRequired " + (s2.getTotalCreditRequired() == 160 && 
															s1.getTotalCreditRequired() == 120));
		
		

	}
}
