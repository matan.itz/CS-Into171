
public class FinalGradeComparator implements Comparator {

	@Override
	public int compare(Object obj1, Object obj2) {
		if(!(obj1 instanceof Grade) || !(obj2 instanceof Grade)) { //the object is not an instance of Grade - cannot compare it
			throw new ClassCastException("Objects must be instance of Grade");
		}
        //casting the objects into 'Grade':
		Grade g1=(Grade) obj1;
		Grade g2=(Grade) obj2;
        //computing the final grades
		int finalGrade1=g1.computeFinalGrade();
		int finalGrade2=g2.computeFinalGrade();
        /* Comparing method:
        Compares its two grade in descending order (highest grade first, lowest grade last)
        If the two grades are equal to each other - return zero
        If finalGrade1 is less than finalGrade2 - return 1 (so it will be placed last)
        If finalGrade1 is greater than finalGrade2 - return -1 (so it will be placed first)
        */
		if(finalGrade1==finalGrade2) {
			return 0;
		}
		else if(finalGrade1<finalGrade2) {
			return 1;
		}
		else {
			return -1;
		}
    }
}
