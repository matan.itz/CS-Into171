
public class StudentNameComparator implements Comparator {

	@Override
	public int compare(Object obj1, Object obj2) {
		if(!(obj1 instanceof Student) || !(obj2 instanceof Student)) { //the object is not an instance of Student - cannot compare it
			throw new ClassCastException("Objects must be instance of Student");
		}
        //casting the objects into 'Student':
		Student s1=(Student)obj1;
		Student s2=(Student)obj2;
		int lastNameCompare=s1.getLastName().compareTo(s2.getLastName()); //calculating the last name compare
		if(lastNameCompare==0) { //the last name is the same, now compare the first name
			return s1.getFirstName().compareTo(s2.getFirstName());
		}
		else{ //the last name is not the same, return the lastNameCompare
			return lastNameCompare;
		}
	}

}
