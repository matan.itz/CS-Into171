
public class ElectiveCourse extends Course { 

	public ElectiveCourse(String name, int number, int credit) {
		super(name, number, credit); //using the parent constructor
	}

	public int computeFinalGrade(int grade){
		if(grade<0 || grade>100) { //wrong input - must be between 0 to 100
            throw new IllegalArgumentException("Grade must be on the scale of 0 to 100");
        }
		if(grade<56) { //if student did not pass the exam, return the grade as it is
			return grade;
		}
		int finalGrade;
		//this is an elective course - add 10% to the grade
    	finalGrade=(int) (grade*1.10);
    	if(finalGrade>100) { //making sure grade won't exceed 100
			return 100;
		}
		else {
			return finalGrade;
		}
	}

}
