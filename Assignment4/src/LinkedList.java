import java.util.Iterator;

public class LinkedList implements List {
	private Link first;
	private Link last;

	public LinkedList(){
		first = null;
		last = null;
	}

	public void sortBy(Comparator comp){
		if(comp==null) { //if the comparator is null - we can't compare the objects
			throw new NullPointerException("Comparator is null");
		}
		if(this.size()<2){ //if the list contains one object - sorting is not relevant
			return;
		}
		Link link1=this.first; //first link to compare
		Link link2=link1.getNext(); //second link to compare
        /*
        We'll go over all the links in the list -
        We are going to compare each link to the links that follow it.
        e.i. - the first one with all the others, than the second one with all the links that follow him,
        the third one with all the links that follow him and so on.
        That is in order to make sure the link is truly sorted
        */
		while(link1.getNext()!=null) { //run through the links as long as the first link to compare has a link that follows that
			while(link2!=null) { //run through the links as long as the second link to compare in not null
				if(comp.compare(link1.getData(), link2.getData())>0) { 
                    //if the first link is bigger than the second one - switch their data
					Object obj1Data=link1.getData();
					Object obj2Data=link2.getData();
					link1.setData(obj2Data);
					link2.setData(obj1Data);
				}
				link2=link2.getNext(); //changing the second link to compare to be the next available link
			}
			link1=link1.getNext(); //changing the first link to compare to be the next link from the start
			link2=link1.getNext(); //changing the second link to compare to be the next available link
		}
	}

	public String toString() {
		String description="<";
		Link myLink=this.first;
        //adding the data of each link to the description:
		while(myLink!=null) {
			description+=myLink.getData().toString();
            if(myLink.getNext()!=null) {
                description+=", ";
            }
			myLink=myLink.getNext();
		}
		description+=">";
		return description;
	}

	public boolean equals(Object other) {  
		if(! (other instanceof LinkedList)) { //object is not an instance of LinkedList, meaning it's not equal
			return false;
		}
		LinkedList otherList=(LinkedList) other;
		if(this.size()!=otherList.size()) { //the objects have different sizes, so they are not the same
			return false;
		}
		Link myLink=this.first;
		Link otherLink=otherList.first;
        //Going over each link in the given lists and checking if their data is the same
		while(myLink!=null && otherLink!=null) {
			if(! myLink.equals(otherLink)) {
				return false;
			}
			myLink=myLink.getNext();
			otherLink=otherLink.getNext();
		}
		return true;
	}

	public void add(int index, Object element) {
		rangeCheck(index);
		nullCheck(element);
		if(index == 0) {
			first = new Link(element, first) ;
			if(last == null)
				last = first ;
		} else {
			Link prev = null ;
			Link curr = first ;
			for(int i=0; i<index; i=i+1) {
				prev = curr ;
				curr = curr.getNext() ;
			}
			Link toAdd = new Link(element, curr);
			prev.setNext(toAdd);
			if(index == size())
				last = toAdd;
		}
	}

	public void add(Object element) {
		nullCheck(element);
		if(isEmpty()){
			first = new Link(element);
			last = first;
		}
		else {
			Link newLast = new Link(element);
			last.setNext(newLast);
			last = newLast;
		}
	}

	@Override
	public int size() {
		int size=0;
        //Going over the list with iterator and increasing the size variable for each link
		Iterator myIterator=iterator();
		while(myIterator.hasNext()){
			size++;
			myIterator.next();
		}
		return size;
	}

	@Override
	public boolean contains(Object element) {
		Link myLink=this.first;
        //Going over the links of the list and checking if the given element is equals to one's data
		while(myLink!=null) {
			if(myLink.getData().equals(element)) { 
				return true;
			}
			myLink=myLink.getNext();
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.first==null; //if the first is null, there are no links in the list
//		return size()==0;
	}

	@Override
	public Object get(int index) {
		if(index<0 || index>=size()) { //index not valid - too small or too big
			 throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
		}
		Link wantedLink=first;
		for(int i=0; i<index; i++) {
			wantedLink=wantedLink.getNext(); //setting the wantedLink to the next in line until reaching the desired index
		}
		return wantedLink.getData();
	}

	@Override
	public Object set(int index, Object element) {
		if(index<0 || index>=size()) { //index not valid - too small or too big
			 throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
		}
		if(element==null) {
			throw new NullPointerException("Element in the list cannot be null");
		}
		Link wantedLink=first;
		for(int i=0; i<index; i++) {
			wantedLink=wantedLink.getNext(); //setting the wantedLink to the next in line until reaching the desired index
		}
		return wantedLink.setData(element); //setting the new data and returning the old data
	}

	@Override
	public Iterator iterator() {
		return new LinkedListIterator(this.first);
	}

	// throws an exception if the given index is not in range
	private void rangeCheck(int index) {
		if(index < 0 || index >= size())
	        throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
	}
	
	// throws an exception if the given element is null
	private void nullCheck(Object element){
		if (element == null)
			throw new NullPointerException();
	}

}
