/**
 * Created by shalev on 27/12/2016.
 */
public class LinkSpec {
    public static void main(String[] args) {
        Link link = new Link("someData");
        Link link2 = new Link("someData2");
        Link linkSameData = new Link("someData");

        // equals
        System.out.println("equals(link, link): " + link.equals(link));
        System.out.println("equals(link, linkSameData): " + link.equals(linkSameData));
        System.out.println("equals(link, link2): " + link.equals(link2));
        System.out.println("equals(link, null): " + link.equals(null));
    }
}
