

import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade03 {
	
	@Test(timeout=2000)
	public void runTest(){
		Course c = new MathElectiveCourse("intro", 202, 5);
		
		Assert.assertEquals("Course: computeFinalGrade()", 75, c.computeFinalGrade(70));
	}
}
