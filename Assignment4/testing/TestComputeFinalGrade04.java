
import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade04 {
	
	@Test(timeout=2000)
	public void runTest(){
		Course c = new CsElectiveCourse("intro", 202, 5);
		
		Assert.assertEquals("Course: computeFinalGrade()", 82, c.computeFinalGrade(70));
	}
}
