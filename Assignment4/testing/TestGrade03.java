

import org.junit.Assert;
import org.junit.Test;

public class TestGrade03 {
	
	@Test(timeout=2000)
	public void runTest(){

		boolean ans = false;
		Course c = new Course("intro", 202, 5);
		
		try {
			Grade g = new Grade(c, -100);
		}
		catch (IllegalArgumentException e) {
			ans = true;
		}
		
		if(!ans)
			Assert.fail("Grade: Expected IllegalArgumentException");
		

	}
}
