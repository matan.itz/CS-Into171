


import org.junit.Assert;
import org.junit.Test;

public class TestLinkedList04 {
	
	@Test(timeout=2000)
	public void runTest(){
		LinkedList lst = new LinkedList();
		boolean ans = false;
		
		try {
			lst.get(10);
		}
		catch (IndexOutOfBoundsException e) {
			ans = true;
		}
		
		if (!ans)
			Assert.fail("Expected IndexOutOfBoundException");
	}
}
