/**
 * Created by shalev on 27/12/2016.
 */
public class StudentNameComparatorSpec {
    public static void main(String[] args) {
        Student student1 = new Student("First", "Last", 123);
        System.out.println("created student1 with name: First Last");

        Student student2 = new Student("First", "LastB", 124);
        System.out.println("created student2 with name: First LastB");
        ComparatorSpec.validateCompare(new StudentNameComparator(), student1, student2);

        student2 = new Student("FirstB", "Last", 124);
        System.out.println("created student2 with name: FirstB Last");
        ComparatorSpec.validateCompare(new StudentNameComparator(), student1, student2);

        student2 = new Student("First", "Last", 124);
        System.out.println("created student2 with name: First Last");
        ComparatorSpec.validateCompare(new StudentNameComparator(), student1, student2);

        try {
            System.out.print("compare(\"student\",\"course\"): ");
            new StudentNameComparator().compare(student1, new Course("course", 123, 1));
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("compare(\"course\", \"student\"): ");
            new StudentNameComparator().compare(new Course("course", 123, 1), student1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
    }
}
