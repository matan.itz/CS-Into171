

import org.junit.Assert;
import org.junit.Test;

public class TestStudentManagement16 {
	
	@Test(timeout=2000)
	public void runTest(){
		StudentManagementSystem sys = new StudentManagementSystem();
		int n = 13;
		for (int i=0; i < n; i++) {
			Course c = new Course("cname", i+1, i+1);
			sys.addCourse(c);
		}
		
		Assert.assertEquals("StudentManagementSystem:  getNumberOfStudents()", n, sys.getNumberOfCourses());
	}
}
