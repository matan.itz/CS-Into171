
import org.junit.Assert;
import org.junit.Test;

public class TestGrade06 {
	
	@Test(timeout=2000)
	public void runTest(){

		boolean ans = false;
		Course c = new Course("intro", 202, 5);
		Grade g = new Grade(c ,80);
		g.setGrade(90);
		
		Assert.assertEquals("Grade: setGrade()", 90, g.getGrade());

	}
}