/**
 * Created by shalev on 27/12/2016.
 */
public class FinalGradeComparatorSpec {
    public static void main(String[] args) {
        Grade grade1 = new Grade(new Course("course1", 123, 1), 80);
        System.out.println("created grade1(Course, 80) - finalGrade=80)");

        Grade grade2 = new Grade(new MathElectiveCourse("course2", 124, 1), 80);
        System.out.println("created grade1(MathElectiveCourse, 80) - finalGrade=85)");

        ComparatorSpec.validateCompare(new FinalGradeComparator(), grade1, grade2);
    }
}
