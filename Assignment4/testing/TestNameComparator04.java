

import org.junit.Assert;
import org.junit.Test;

public class TestNameComparator04 {
	
	@Test(timeout=2000)
	public void runTest(){
		boolean ans = false;
		Comparator nameComp = new StudentNameComparator();
		Student s1 = new Student("Alice", "Bob", 1);
		
		try {
			nameComp.compare(s1, new Object());
		}
		catch (ClassCastException e) {
			ans = true;
		}
		
		if (!ans)
			Assert.fail("StudentNameComparator: Expected ClassCastException");
	}
}
