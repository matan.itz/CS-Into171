

import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade05 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice", "Bob", 777);
		Course c1 = new CsElectiveCourse("intro", 202, 50);
		Course c2 = new MathElectiveCourse("math", 1, 10);
		Course c3 = new Course("Course", 5, 60);
		s.registerTo(c1);
		s.registerTo(c2);
		s.registerTo(c3);
		s.addGrade(c1, 70);
		s.addGrade(c2, 80);
		s.addGrade(c3, 80);
	
		Assert.assertEquals("Student: computeFinalGrade()", 81.25, s.computeFinalGrade(), 0.001);
	}	
}
