

import org.junit.Assert;
import org.junit.Test;

public class TestStudent03 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		
		Assert.assertEquals("Student: getId()", 1, s.getId());
	}
}
