

import org.junit.Assert;
import org.junit.Test;

public class TestCourse07 {
	
	@Test(timeout=2000)
	public void runTest(){

		Course c = new Course("intro" , 202, 5);
		
		Assert.assertEquals("Course: getCourseCredit()", 5, c.getCourseCredit());

	}
}
