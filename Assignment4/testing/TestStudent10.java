

import org.junit.Assert;
import org.junit.Test;

public class TestStudent10 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		Course c = new Course("intro", 202, 80);
		s.registerTo(c);
		
		Assert.assertEquals("Student: registerTo()", false, s.registerTo(c));
	}
}
