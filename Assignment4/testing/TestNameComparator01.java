

import org.junit.Assert;
import org.junit.Test;

public class TestNameComparator01 {
	
	@Test(timeout=2000)
	public void runTest(){
		boolean ans = false;
		Comparator nameComp = new StudentNameComparator();
		Student s1 = new Student("Alice", "Bob", 1);
		Student s2 = new Student("Bob", "Alice", 10);
		
		Assert.assertEquals("StudentNameComparator: compare()", true, nameComp.compare(s2, s1) < 0);
		
	}
}
