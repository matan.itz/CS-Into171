


import org.junit.Assert;
import org.junit.Test;

public class TestLink03 {
	
	@Test(timeout=2000)
	public void runTest(){
		Link l1 = new Link(new Integer(5));
		Object o = new Object();
		
		Assert.assertEquals("Link: setData()", false, l1.equals(o));
	}
}
