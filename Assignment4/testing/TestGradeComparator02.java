

import org.junit.Assert;
import org.junit.Test;

public class TestGradeComparator02 {
	
	@Test(timeout=2000)
	public void runTest(){
		boolean ans = false;
		Comparator avgComp = new StudentGradeAverageComparator();
		Student s1 = new Student("Alice", "Bob", 1);
		Student s2 = new Student("Bob", "Alice", 10);
		Course c1 = new Course("intro", 101, 60);
		Course c2 = new Course("intro2", 121, 120);
		
		s1.registerTo(c1);
		s1.registerTo(c2);
		s2.registerTo(c1);
		s2.registerTo(c2);
		
		s1.addGrade(c1, 99);
		s1.addGrade(c2, 99);
		s2.addGrade(c1, 100);
		s2.addGrade(c2, 99);
		
		System.out.println(s1.calculateAverage()); // 99.0
		System.out.println(s2.calculateAverage()); // 99.33333333
		
		Assert.assertEquals("StudentGradeAverageComparator: compare()", false, avgComp.compare(s2, s1) == 0);
		
		
		
	}
}
