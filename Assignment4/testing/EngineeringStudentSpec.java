/**
 * Created by shalev on 27/12/2016.
 */
public class EngineeringStudentSpec {
    public static void main(String[] args) {
        EngineeringStudent student = new EngineeringStudent("First", "Last", 123);
        System.out.println("created EngineeringStudent (\"First\", \"Last\", 123)");
        StudentSpec.validate(student);
    }
}
