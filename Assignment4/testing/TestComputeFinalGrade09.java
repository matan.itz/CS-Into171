

import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade09 {
	
	@Test(timeout=2000)
	public void runTest(){
		Comparator fGradeComp = new FinalGradeComparator();
		StudentManagementSystem sys = new StudentManagementSystem();
		
		Course c1 = new CsElectiveCourse("intro", 202, 50);
		Course c2 = new MathElectiveCourse("math", 1, 10);
		Course c3 = new Course("Course", 5, 60);
		Course c4 = new Course("Ecourse", 999, 40);
		
		Grade g1 = new Grade(c1, 60);
		Grade g2 = new Grade(c1, 70);
		Grade g3 = new Grade(c1, 80);
		Grade g4 = new Grade(c1, 75);
		
		
		Assert.assertEquals("FinalGradeComparator: compare()", true, (fGradeComp.compare(g2, g1) < 0));
	}
}
