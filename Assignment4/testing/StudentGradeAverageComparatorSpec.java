/**
 * Created by shalev on 27/12/2016.
 */
public class StudentGradeAverageComparatorSpec {
    public static void main(String[] args) {
        Student student1 = new Student("First", "Last", 123);
        Course course1 = new Course("course1", 123, 1);
        student1.registerTo(course1);
        student1.addGrade(course1, 80);
        System.out.println("created student1 with grade(Course, 80)");

        Student student2 = new Student("FirstB", "LastB", 124);
        Course course2 = new MathElectiveCourse("course2", 124, 1);
        Course course3 = new MathElectiveCourse("course3", 125, 1);
        student2.registerTo(course2);
        student2.registerTo(course3);
        student2.addGrade(course2, 70);
        student2.addGrade(course3, 100);
        System.out.println("created student1 with grade(MathElectiveCourse, 70) and grade(MathElectiveCourse, 100), average=75");

        ComparatorSpec.validateCompare(new StudentGradeAverageComparator(), student1, student2);
    }
}
