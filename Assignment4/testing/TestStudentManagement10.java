

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;

public class TestStudentManagement10 {
	
	@Test(timeout=2000)
	public void runTest(){
		StudentManagementSystem sys = new StudentManagementSystem();
		boolean ans = false;
		Student s1 = new Student("name", "A", 1);
		Student s2 = new Student("name", "C", 3);
		Student s3 = new Student("name", "Z", 26);
		Student s4 = new Student("name", "B", 2);
		
		sys.addStudent(s1);
		sys.addStudent(s2);
		sys.addStudent(s3);
		sys.addStudent(s4);
		
		try {
			LinkedList value = sys.getFirstKStudents(new StudentNameComparator(), 200);
		}
		catch (IllegalArgumentException e) {
			ans = true;
		}
		
		if (!ans)
			Assert.fail("StudentManagementSystem: getFirstKStudents() - Expected IllegalArgumentException");
		
	}
	
	private static boolean subSet(LinkedList l1, LinkedList l2) {
		Iterator iter1 = l1.iterator();
		while (iter1.hasNext()) {
			Object toFind = iter1.next();
			boolean found = false;
			Iterator iter2 = l2.iterator();
			
			while (iter2.hasNext() && !found) {
				found = toFind.equals(iter2.next());
			}
			if (!found)
				return false;
		}
		return true;
	}
}
