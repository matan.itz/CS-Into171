/**
 * Created by shalev on 25/12/2016.
 */
public class CourseSpec {
    public static void main(String[] args) {
        // constructor validations
        contructorValidations();

        Course course = new Course("course", 123, 1);
        System.out.println("created Course (\"course\", 123, 30)");
        validate(course);
    }


    public static void validate(Course course) {

        // getters
        System.out.println("getCourseName(): " + course.getCourseName());
        System.out.println("getCourseNumber(): " + course.getCourseNumber());
        System.out.println("getCourseCredit(): " + course.getCourseCredit());

        // equals()
        System.out.println("equals(self): " + course.equals(course));
        System.out.println("equals(other with same number): " + course.equals(new Course("course2", 123, 2)));
        System.out.println("equals(other): " + course.equals(new Course("course2", 124, 1)));
        System.out.println("equals(student): " + course.equals(new Student("s", "s", 123)));
        System.out.println("equals(null): " + course.equals(null));

        // computeFinalGrade()
        try {
            System.out.print("computeFinalGrade(-1): ");
            course.computeFinalGrade(-1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        try {
            System.out.print("computeFinalGrade(101): ");
            course.computeFinalGrade(101);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        System.out.println("computeFinalGrade(50): " + course.computeFinalGrade(50));
        System.out.println("computeFinalGrade(50): " + course.computeFinalGrade(80));
        System.out.println("computeFinalGrade(100): " + course.computeFinalGrade(100));
    }

    public static void contructorValidations(){
        try {
            System.out.print("new Course(\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 \",1,1): ");
            new Course("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ", 1, 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Course(null,1,1): ");
            new Course(null, 1, 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Course(\"course@\",1,1): ");
            new Course("course@", 1, 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Course(\"course@\",1,1): ");
            new Course("course@", 1, 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Course(\"\",1,1): ");
            new Course("", 1, 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Course(\"course\",0,1): ");
            new Course("course", 0, 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("new Course(\"course\",1,0): ");
            new Course("course", 1, 0);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
    }
}
