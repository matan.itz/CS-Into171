
import org.junit.Assert;
import org.junit.Test;

public class TestGrade09 {
	
	@Test(timeout=2000)
	public void runTest(){

		Course c = new Course("intro", 202, 5);
		Grade g = new Grade(c ,80);
		Grade g2 = new Grade(new Course("intro", 222, 5), 80);
		
		Assert.assertEquals("Grade: equals()", false, g.equals(g2));
	}
}
