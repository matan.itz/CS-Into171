

import org.junit.Assert;
import org.junit.Test;

public class TestCourse06 {
	
	@Test(timeout=2000)
	public void runTest(){

		Course c = new Course("intro" , 202, 5);
		
		Assert.assertEquals("Course: getNumber()", 202, c.getCourseNumber());

	}
}
