

import org.junit.Assert;
import org.junit.Test;

public class TestStudent13 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		Course c = new Course("intro", 202, 80);
		s.registerTo(c);
		s.addGrade(c, 90);
		
		Assert.assertEquals("Student: setGrade()", 90, s.setGrade(c, 100));
	}
}
