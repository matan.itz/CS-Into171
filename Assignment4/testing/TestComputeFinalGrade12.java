import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade12 {
	
	@Test(timeout=2000)
	public void runTest(){
		Comparator fGradeComp = new FinalGradeComparator();
		boolean ans = false;
		Object o = new Object();
		try {
			Assert.assertEquals("FinalGradeComparator: compare()", true, (fGradeComp.compare(o, o) == 0));
		}
		catch(ClassCastException e) {
			ans = true;
		}
		
		if (!ans)
			Assert.fail("FinalGradeComparator: Expected ClassCastException");
		
	}
}
