

import org.junit.Assert;
import org.junit.Test;

public class TestGradeComparator01 {
	
	@Test(timeout=2000)
	public void runTest(){
		boolean ans = false;
		Comparator avgComp = new StudentGradeAverageComparator();
		Student s1 = new Student("Alice", "Bob", 1);
		Student s2 = new Student("Bob", "Alice", 10);
		Course c1 = new Course("intro", 101, 5);
		Course c2 = new Course("intro2", 121, 5);
		
		s1.registerTo(c1);
		s1.registerTo(c2);
		s2.registerTo(c1);
		s2.registerTo(c2);
		
		s1.addGrade(c1, 100);
		s1.addGrade(c2, 80);
		s2.addGrade(c1, 90);
		s2.addGrade(c2, 70);
		
		Assert.assertEquals("StudentGradeAverageComparator: compare()", true, avgComp.compare(s1, s2) < 0);
		
		
		
	}
}
