


import org.junit.Assert;
import org.junit.Test;

public class TestLinkedList07 {
	
	@Test(timeout=2000)
	public void runTest(){
		LinkedList lst = new LinkedList();
		LinkedList lst2 = new LinkedList();
		for (int i=0; i < 5; i++)
		{
			lst.add(new Integer(i));
			lst2.add(new Integer(i));
		}
		lst.add(new Integer(100));
	
		Assert.assertEquals("LinkedList: equals()", false, lst.equals(lst2));
	}
}
