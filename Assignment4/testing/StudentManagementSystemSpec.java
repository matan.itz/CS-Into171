/**
 * Created by shalev on 27/12/2016.
 */
public class StudentManagementSystemSpec {
    public static void main(String[] args) {
        StudentManagementSystem sms = new StudentManagementSystem();

        // addStudent() & getNumberOfStudents()
        Student student = new Student("First", "Last", 123);
        System.out.println("addStudent(student): " + sms.addStudent(student));
        System.out.println("getNumberOfStudents(): " + sms.getNumberOfStudents());

        System.out.println("addStudent(student again): " + sms.addStudent(student));
        System.out.println("getNumberOfStudents(): " + sms.getNumberOfStudents());

        System.out.println("addStudent(student with same id): " + sms.addStudent(new Student("FirstB", "LastB", 123)));
        System.out.println("getNumberOfStudents(): " + sms.getNumberOfStudents());

        // addCourse() & getNumberOfCourses()
        Course course = new Course("course", 123, 1);
        System.out.println("addCourse(course): " + sms.addCourse(course));
        System.out.println("getNumberOfCourses(): " + sms.getNumberOfCourses());

        System.out.println("addCourse(course again): " + sms.addCourse(course));
        System.out.println("getNumberOfCourses(): " + sms.getNumberOfCourses());

        System.out.println("addCourse(course with same number): " + sms.addCourse(new Course("course2", 123, 2)));
        System.out.println("getNumberOfCourses(): " + sms.getNumberOfCourses());

        // register() & addGradeToStudent()
        System.out.println("addGradeToStudent(student, course, 100), before register: " + sms.addGradeToStudent(student, course, 100));
        System.out.println("register(student, course): " + sms.register(student, course));
        System.out.println("addGradeToStudent(student, course, 100): " + sms.addGradeToStudent(student, course, 100));
        System.out.println("register(student, course) again: " + sms.register(student, course));
        System.out.println("addGradeToStudent(student, course, 100) again: " + sms.addGradeToStudent(student, course, 100));

        System.out.println("register(other student, course): " + sms.register(new Student("FirstB", "LastB", 124), course));
        System.out.println("register(student, other course): " + sms.register(student, new Course("course2", 123, 1)));

        // getFirstKStudents()
        Student student2 = new Student("AFirst", "Last", 124);
        sms.addStudent(student2);
        System.out.println("getFirstKStudents(comp, 0): " + sms.getFirstKStudents(new StudentNameComparator(), 0).size());
        System.out.println("getFirstKStudents(comp, 1): " + sms.getFirstKStudents(new StudentNameComparator(), 1).size());
        Student firstStudent = (Student) sms.getFirstKStudents(new StudentNameComparator(), 2).get(0);
        System.out.println("getFirstKStudents(comp, 2): " + firstStudent.getFirstName());
        try {
            System.out.print("getFirstKStudents(null,0): ");
            sms.getFirstKStudents(null, 1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }
        try {
            System.out.print("getFirstKStudents(comp,-1): ");
            sms.getFirstKStudents(new StudentNameComparator(), -1);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        try {
            System.out.print("getFirstKStudents(comp,3): ");
            sms.getFirstKStudents(new StudentNameComparator(), 3);
            System.out.println("No Exception");
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
        }

        // computeFinalGrade()
        System.out.println("computeFinalGrade(student): " + sms.computeFinalGrade(student));
    }
}
