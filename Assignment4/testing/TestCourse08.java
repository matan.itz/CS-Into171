

import org.junit.Assert;
import org.junit.Test;

public class TestCourse08 {
	
	@Test(timeout=2000)
	public void runTest(){

		Course c = new Course("intro" , 202, 5);
		
		Assert.assertEquals("Course: getName()", "intro", c.getCourseName());

	}
}
