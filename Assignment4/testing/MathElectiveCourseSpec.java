/**
 * Created by shalev on 27/12/2016.
 */
public class MathElectiveCourseSpec {
    public static void main(String[] args) {
        MathElectiveCourse course = new MathElectiveCourse("course", 123, 1);
        System.out.println("created MathElectiveCourse (\"course\", 123, 30)");
        CourseSpec.validate(course);
    }
}
