

import org.junit.Assert;
import org.junit.Test;

public class TestStudent08 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		Student s2 = new Student("Bob", "Alice", 10);
		
		Assert.assertEquals("Student: equals()", false, s.equals(s2));
	}
}
