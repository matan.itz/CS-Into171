import org.junit.Assert;
import org.junit.Test;

public class TestComputeFinalGrade13 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice", "Bob", 202);
		Course c1 = new Course("intro", 202, 100);
		Course c2 = new Course("course", 111 , 20);
		Course c3 = new Course("course", 221 , 20);
		
		s.registerTo(c1);
		s.registerTo(c2);
		s.registerTo(c3);
		
		s.addGrade(c1, 100);
		s.addGrade(c2, 60);
		s.addGrade(c3, 80);
		
		System.out.println(s.computeFinalGrade());
		
		Assert.assertEquals("Student: computeFinalGrade", 96.66, s.computeFinalGrade(), 0.01);
		
	}
}
