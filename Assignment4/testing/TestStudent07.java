

import org.junit.Assert;
import org.junit.Test;

public class TestStudent07 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		Student s2 = new Student("Bob", "Alice", 1);
		
		Assert.assertEquals("Student: equals()", true, s.equals(s2));
	}
}
