

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;

public class TestStudentManagement17 {
	
	@Test(timeout=2000)
	public void runTest(){
		StudentManagementSystem sys = new StudentManagementSystem();
		int n = 10;
		Student s1 = new Student("A", "A", 1);
		Student s2 = new Student("C", "C", 3);
		Student s3 = new Student("Z", "Z", 26);
		Student s4 = new Student("B", "A", 2);
		
		sys.addStudent(s1);
		sys.addStudent(s2);
		sys.addStudent(s3);
		sys.addStudent(s4);
		
		LinkedList value = sys.getFirstKStudents(new StudentNameComparator(), 3);
		LinkedList expected = new LinkedList();
		expected.add(s1);
		expected.add(s4);
		expected.add(s2);
		
		System.out.println(value);
		boolean ans = subSet(value, expected) && subSet(expected, value);
		
		Assert.assertEquals("StudentManagementSystem: getFirstKStudents()", true, ans);
	}
	
	private static boolean subSet(LinkedList l1, LinkedList l2) {
		Iterator iter1 = l1.iterator();
		while (iter1.hasNext()) {
			Object toFind = iter1.next();
			boolean found = false;
			Iterator iter2 = l2.iterator();
			
			while (iter2.hasNext() && !found) {
				found = toFind.equals(iter2.next());
			}
			if (!found)
				return false;
		}
		return true;
	}
}
