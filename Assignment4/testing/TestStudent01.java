

import org.junit.Assert;
import org.junit.Test;

public class TestStudent01 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		
		Assert.assertEquals("Student: getLastName()", "Bob", s.getLastName());
	}
}
