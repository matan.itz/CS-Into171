/**
 * Created by shalev on 27/12/2016.
 */
public class CsElectiveCourseSpec {
    public static void main(String[] args) {
        CsElectiveCourse course = new CsElectiveCourse("course", 123, 1);
        System.out.println("created CsElectiveCourse (\"course\", 123, 30)");
        CourseSpec.validate(course);
    }
}
