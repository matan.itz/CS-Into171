

import org.junit.Assert;
import org.junit.Test;

public class TestStudent12 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		Course c = new Course("intro", 202, 80);
		s.registerTo(c);
		s.addGrade(c, 100);
		
		Assert.assertEquals("Student: addGrade()", false, s.addGrade(c, 100));
	}
}
