

import org.junit.Assert;
import org.junit.Test;

public class TestLinkedList08 implements Comparator{
	
	@Test(timeout=2000)
	public void runTest(){
		LinkedList lst = new LinkedList();
		LinkedList lst2 = new LinkedList();
		for (int i=0; i < 5; i++)
		{
			lst2.add(new Integer(i));
		}
		lst.add(new Integer(3));
		lst.add(new Integer(1));
		lst.add(new Integer(4));
		lst.add(new Integer(0));
		lst.add(new Integer(2));
		
		lst.sortBy(this);
		
		Assert.assertEquals("LinkedList: sortBy()", lst2, lst);
	}
	
	public int compare(Object o1, Object o2) {
		return (Integer)o1 - (Integer)o2;
	}
}
