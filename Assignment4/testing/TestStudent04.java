

import org.junit.Assert;
import org.junit.Test;

public class TestStudent04 {
	
	@Test(timeout=2000)
	public void runTest(){
		Student s = new Student("Alice" , "Bob", 1);
		Course c = new Course("intro", 202, 80);
		
		Assert.assertEquals("Student: isRegisteredTo()", false, s.isRegisteredTo(c));
	}
}
