
public class Test_PartOne {
	public static void main (String [] args){
		
		
		//================== test printBoard ==================
		int[] board0 = {1, 3, 0, 2};
		NQueens.printBoard(board0);
		System.out.println("");
		/*
		 * should print: 
		 
		 * Q * *  
		 * * * Q  
		 Q * * *  
		 * * Q *  
	
		 */
		int[] board00 = {1, 3, 0, 0};
		NQueens.printBoard(board00);
		System.out.println("");

		/*
		 * should print: 

		 * Q * *  
		 * * * Q  
		 Q * * *  
		 Q * * * 
	
		 */
		
		
		//================== test isLegalSolution ==================

		int[] board1 = {1, 3, 0, 2};
		boolean ans1 = NQueens.isLegalSolution(board1, board1.length);
		System.out.println("ans1 is " + ans1 +",  should be true");
		
		int[] board2 = {0,2,1,3};
		boolean ans2 = NQueens.isLegalSolution(board2, board2.length);
		System.out.println("ans2 is " + ans2 + ", should be false");
		
		int[] board3 = {0};
		boolean ans3 = NQueens.isLegalSolution(board3, board3.length);
		System.out.println("ans3 is " + ans3 +",  should be true");
		
		int[] board4 = {1, 0, 2};
		boolean ans4 = NQueens.isLegalSolution(board4, board4.length);
		System.out.println("ans4 is " + ans4 +",  should be false");
		
		int[] board5 = {};
		boolean ans5 = NQueens.isLegalSolution(board5, 3);
		System.out.println("ans5 is " + ans5 +",  should be true");
		
		//================== test addQueen ==================
		
		int[] board11 = {1, 3, 5, 0, 5, 0};
		boolean ans11 = NQueens.addQueen(board11, 4,2);
		System.out.println("ans11 is " + ans11 +",  should be true");
		
		int[] board12 = {1, 3, 5, 0, 5, 0};
		boolean ans12 = NQueens.addQueen(board12, 3,4 );
		System.out.println("ans12 is " + ans12 +", should be false");
		
		int[] board13 = {1, 3, 5, 0, 5, 0};
		boolean ans13 = NQueens.addQueen(board13, 3,3);
		System.out.println("ans13 is " + ans13 +", should be false");
		
		
		//================== test nQueens(int n) ================== 
		
		int[] board20 = NQueens.nQueens(1);
		NQueens.printBoard(board20);
		System.out.println(""); 
		/*
		 
		 * should print:
		 Q
		 
		 */
		
		
		int[] board21 = NQueens.nQueens(2);
		NQueens.printBoard(board21);
		System.out.println("");
		/*
		 
		  should print:

		 There is no solution

		 */
		
		int[] board22 = NQueens.nQueens(4);
		NQueens.printBoard(board22);
		System.out.println("");
		/*
		 
		 * should print:
		
		 * Q * *  
		 * * * Q  
		 Q * * *  
		 * * Q *  
		 
		 (or another legal board or size 4*4)
		 */
		int[] board23 = NQueens.nQueens(6);
		NQueens.printBoard(board23);
		System.out.println("");
		
		/*
		 
		 * should print:
		 
		* Q * * * *  
		* * * Q * *  
		* * * * * Q  
		Q * * * * *  
		* * Q * * *  
		* * * * Q *  
		
		 (or another legal board or size 6*6)
		 */
		
		//================== test nQueens(int[] board, int row, int col) ==================
		
		int[] board30 = {1,3,5,4,3,3};
		boolean ans30 = NQueens.nQueens(board30, 3, 0);
		System.out.println("ans30 is " + ans30 +", should be true");

		int[] board31 = {1,3,5,4,3,3};
		boolean ans31 = NQueens.nQueens(board31, 3, 2);
		System.out.println("ans31 is " + ans31 +", should be false");
		
		int[] board32 = {1,3,5,4,3,3};
		boolean ans32 = NQueens.nQueens(board32, 3, 5);
		System.out.println("ans32 is " + ans32 +", should be false");
	
	}

}
