public class RegistrationSystem {

	private static int MAX_NUMBER_OF_STUDENTS=500;
	private static int MAX_NUMBER_OF_COURSES=500;
	private Student[] studentsArray;
	private Course[] coursesArray;
	private int numberOfStudents;
	private int numberOfCourses;

	public RegistrationSystem(){
		this.studentsArray=new Student[MAX_NUMBER_OF_STUDENTS];
		this.coursesArray=new Course[MAX_NUMBER_OF_COURSES];
		this.numberOfStudents=0;
		this.numberOfCourses=0;
	}

	public boolean addStudent(Student student){
		//If the student is already on the system or there are already the max number of students, you cannot add this student
		if(this.numberOfStudents>=MAX_NUMBER_OF_STUDENTS || this.isStudnetExist(student)) {
			return false;
		}
		//Else - adding the student to the array and increasing the number of students
		this.studentsArray[numberOfStudents]=student;
		this.numberOfStudents++;
		return true;
	}

	public boolean addCourse(Course course){
		//If the course is already on the system or there are already the max number of courses, you cannot add this course
		if(this.numberOfCourses>=MAX_NUMBER_OF_COURSES || this.isCourseExists(course)) {
			return false;
		}
		//Else - adding the course to the array and increasing the number of courses
		this.coursesArray[numberOfCourses]=course;
		this.numberOfCourses++;
		return true;

	}

	public boolean register(Student student, Course course){
		//If the student or the course doesn't exist or if the student is already registered to that course, return false
		if(!isStudnetExist(student) || !isCourseExists(course) || student.isRegisteredTo(course)) {			
			return false;
		}
		//Else - add that course to the student's array
		student.registerTo(course);
		return true;
	}



	public boolean[][] findExamConflicts(){
		boolean[][] examConflicts=new boolean[this.numberOfCourses][this.numberOfCourses];
		//Going over all courses, taking two different courses at a time
		for(int i=0; i<this.numberOfCourses; i++) {
			Course firstCourse=this.coursesArray[i];
			for(int j=i+1; j<this.numberOfCourses; j++) {
				Course secondCourse=this.coursesArray[j];
				//Going over all students and checking if one of them takes both courses - if so, there's a conflict
				for(int studentIndex=0; studentIndex<this.numberOfStudents; studentIndex++) {
					Student myStudent=this.studentsArray[studentIndex];
					if(myStudent.isRegisteredTo(firstCourse) && myStudent.isRegisteredTo(secondCourse)) {
						examConflicts[i][j]=true;
					}
				}
			}
		}
		return examConflicts;
	}
	
	private boolean isStudnetExist(Student studentToCheck) {
		//Going over all students and checking if the given student is registered in the system
		for(int i=0; i<this.numberOfStudents; i++) {
			Student myStudent=this.studentsArray[i];
			if(myStudent.isEqualTo(studentToCheck)) {
				return true;
			}
		}
		return false;
	}
	private boolean isCourseExists(Course courseToCheck) {
		//Going over all courses and checking if the given course is registered in the system
		for(int i=0; i<this.numberOfCourses; i++) {
			Course myCourse=this.coursesArray[i];
			if(myCourse.isEqualTo(courseToCheck)) {
				return true;
			}
		}
		return false;
	}
}
