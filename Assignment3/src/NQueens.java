public class NQueens {

	public static void printBoard(int[] board){
		if(board.length==0) {
			System.out.println("There is no solution");
		}
		else {
			for(int row=0; row<board.length; row++) { //Go over all rows
				for(int col=0; col<board.length; col++) { //Go over all columns
					if(board[row]!=col) { // if the column index is not store inside the row cell - there is no queen there
						System.out.print("* ");
					}
					else { // otherwise - there's a queen
						System.out.print("Q ");
					}
				}
				System.out.println();
			}
		}
	}

	public static boolean isLegalSolution(int[] board, int n){
		if(board.length==0 || board.length==1) { //for board of zero or one - there is always a solution
			return true;
		}
		if(board.length==2 || board.length==3) { //for board of zero or one - there is always no solution
			return false;
		}
		for(int i=0; i<board.length; i++) { //goes over the board and checks each queen is located in a legal place
			if(!canPlaceQueen(board, i, board[i])) {
				return false;
			}
		}
		return true;
	}

	public static boolean addQueen(int[] board, int row, int col){
		//Checks if a queen is able to be added to the desired location
		//If so, add the queen and return true
		if(canPlaceQueen(board, row, col)) {
			board[row]=col;
			return true;
		}
		return false;
	}

	public static int[] nQueens(int n) {
		int[] board=new int[n];
		if(nQueens(board, 0, 0)) { //calls the recursive function and starts from the very beginning
			return board;
		}
		else{
			return new int[0];
		}
	}
	
	public static boolean nQueens(int[] board, int row, int col) {
		if(row==board.length) { //when you get to the last row - you're done and return true
			return true;
		}
		if(col==board.length) { //when you get to the last column and couldn't add a queen, you're out of luck and return false
			return false;
		}
		if(!addQueen(board, row, col)) { //if couldn't add queen, try the next row
			return nQueens(board, row, col+1);
		}
		else{ //if added a queen, try the next row or try the next column
			return nQueens(board, row+1, 0) || nQueens(board, row, col+1);
		}
	}
	
    public static boolean canPlaceQueen(int[] board, int row, int col) {
        for (int i = 0; i < row; i++) {
        	//cannot add queen if there is a queen located at the same column, or on the diagonal
            if (board[i] == col || (i - row) == (board[i] - col) || (i - row) == (col - board[i])) {
                return false;
            }
        }
        return true;
    }
	
}
