public class Task3 {
	public static boolean isLegalSchedule(int[] schedule, String[] students, String[] courses, int[][] studentCourses, int k) {
		//Checking courses scheduling:
		for(int i=0; i<schedule.length; i++) {
			if(schedule[i]<0 || schedule[i]>=k) { //There's an exam on illegal day
				return false;
			}
		}
		
		//Checking students won't have more than one exam per day:
		for(int student=0; student<students.length; student++) {
			for(int day=0; day<k; day++) {
				if(examsPerDay(student, day, studentCourses, schedule)>1) {
					return false;
				}
			}
		}
		return true;
	}
	
	public static int[] findConflictingStudent(int[] schedule, String[] students, String[] courses, int[][] studentCourses, int k) {
		int[] conflictArr=new int[4];//Creating an array for conflicts
		int courseID;
		boolean firstExamModified=false;
		boolean secondExamModified=false;
		for(int student=0; student<studentCourses.length; student++) {//Going over all students
			for(int day=0; day<k; day++) {//Going over all days
				if(examsPerDay(student, day, studentCourses, schedule)>1) {//Checking if there are more than one exam per day
					//If so, filling the array with information about the conflict
					conflictArr[0]=day;
					conflictArr[1]=student;
					for(int course=0; course<studentCourses[student].length; course++) {//Going over the courses
						courseID=studentCourses[student][course];
						if(schedule[courseID]==day) {//Checking if that course is on that day
							if(!firstExamModified) {
								conflictArr[2]=courseID;
								firstExamModified=true;
							}
							else if(!secondExamModified) {
								conflictArr[3]=courseID;
								secondExamModified=true;
							}
						}
					}
					return conflictArr;
				}
			}
		}
		
		return new int[0];
	}
	
	public static void printConflictingStudent(int[] schedule, String[] students, String[] courses, int[][] studentCourses, int k) {
		int[] conflictArr=findConflictingStudent(schedule, students, courses, studentCourses, k);
		//Printing the conflict array
		if(conflictArr.length==4) {
			int dayNum=conflictArr[0];
			int studentNum=conflictArr[1];
			int examNum1=conflictArr[2];
			int examNum2=conflictArr[3];
			System.out.println(students[studentNum]+" cannot take exams in "+courses[examNum1]+" and "+courses[examNum2]+" as scheduled on day "+dayNum);
		}
	
	}
	
	public static int examsPerDay(int student, int day, int[][] studentCourses, int[] schedule) {
		int examsPerDay=0;
		int courseID;
		//Going over each course the student takes and checking if it is scheduled to the desired day, and adding that to a counter
		for(int course=0; course<studentCourses[student].length; course++) {
			courseID=studentCourses[student][course];
			if(schedule[courseID]==day) {
				examsPerDay++;
			}
		}
		
		return examsPerDay;
	}
}
