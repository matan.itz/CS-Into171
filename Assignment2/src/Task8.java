public class Task8 {
	public static int[] convertOutput(int[][] variableNames, boolean[] assignment) {
		int[] schedule = new int[variableNames.length];

		for(int i=0; i<variableNames.length; i++) {
			for(int j=0; j<variableNames[i].length; j++) {
				//If the solution for index of variableNames[i][j] is true - 
				//Course `i` will be on day `j` 
				if(assignment[variableNames[i][j]]) {
					schedule[i]=j;
				}
			}
		}

		return schedule;
	}
}
