public class Task2 {
	public static void printSchedule(int[] schedule, String[] courses) {
		
		if(schedule==null || schedule.length!=courses.length) {
			throw new IllegalArgumentException("The input does not follow needed criterias");
		}
		
		boolean hasExams=false; // a boolean to check if a day has exams in that day
		int maxDay=getMaxDay(schedule);
		for(int day=0; day<=maxDay; day++) {
			hasExams=false;
			System.out.print("Day "+day+": ");
			for(int i=0; i<courses.length; i++) {
				if(schedule[i]==day) { //the course is in the specific day
					hasExams=true;
					if(hasDuplicates(i, schedule)) { //checking for another course in that day, to add a comma
						System.out.print(courses[i]+", ");
					}
					else {
						System.out.println(courses[i]);
					}
				}
			}
			if(!hasExams) {
				System.out.println();
			}
		}
	
	}
	
	public static boolean hasDuplicates(int i, int[] arr) {
		for(int j=i+1; j<arr.length; j++) {
			if(arr[i]==arr[j]) {
				return true;
			}
		}
		return false;
	}
	
	public static int getMaxDay(int[] schedule) {
		int maxDay=Integer.MIN_VALUE;
		//Going over the schedule and finding the max value (represents the day)
		for(int i=0; i<schedule.length; i++) {
			if(schedule[i]>maxDay) {
				maxDay=schedule[i];
			}
		}
		return maxDay;
	}
}
