public class Task9 {
	public static int[] solveETP(String[] students, String[] courses, int[][] studentCourses, int k) {
		//Checking if data is valid -
		if(!Task0.legalData(students, courses, studentCourses, k)) {
			throw new IllegalArgumentException("The input does not follow needed criterias");
		}
		SATSolver.init(courses.length*k); //Initializing SAT Solver
		int[] schedule = null;
		int[][] cnfTable=Task6.variableTable(courses.length, k);//Building variables table
		Task7.convertInput(cnfTable, students, courses, studentCourses, k); //Adding clauses to the SAT Solver according the variables table 
		boolean[] solution=SATSolver.getSolution();//Getting solution
		if(solution.length>0) {//If solution exists -
			schedule=Task8.convertOutput(cnfTable, solution);//Converting the CNF to exam schedule
			if(!Task3.isLegalSchedule(schedule, students, courses, studentCourses, k)) {//Making sure the schedule is valid
				throw new AssertionError("The schedule is not valid!");
			}
		}
		if(schedule!=null && schedule.length>0) {
			return schedule;
		}
		else{
			return new int[0];
		}
	}
}
