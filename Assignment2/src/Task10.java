public class Task10 {
	public static int[] solveMinDaysETP(String[] students, String[] courses, int[][] studentCourses) {
		//Checking if data is valid -
		if(!Task0.legalData(students, courses, studentCourses, 1)) {
			throw new IllegalArgumentException("The input does not follow needed criterias");
		}
		//Going over all `k` values - from 1 to number of courses, to see where a possible solution is available
		for(int k=1; k<=courses.length; k++) {
			int[] result=Task9.solveETP(students, courses, studentCourses, k);
			if(result.length!=0) {
				return result;
			}
		}
		return new int[0];
	}
}
