# CS-Intro171 Assignments#

This repository includes all the assignments that we were given during this semester.

For each assignment you will be able to find:
* The instructions
* My source code
* Some test files (see details below)

### Credits for the Test Files ###

* Some of the test files are the ones we were given by the course's staff
* Files that their names end with "Spec" are test files that were written by `Shalev Shalit`, as found on [his site](http://cstests.herokuapp.com/)
* Assignment5Test is a test file that was written by `Nadav Brama`
* Pickatest (Assignment 5) is a test file that was written by `Aviv Metz` and `Hod Alpert`

### My Assignments' State ###
Here is the state of my assignments, if you want to take a look at my code and see if it's any good:

Assignment Number  |  My Grade       | Grader Note
------------------ | --------------- | -------------
Assignment 1       | 106 out of 110  | https://goo.gl/4FxvBb
Assignment 2       | 100 out of 100  | https://goo.gl/VOs0J4
Assignment 3       | 90 out of 100 * | https://goo.gl/B3xoBC
Assignment 4       | 95 out of 100  *| https://goo.gl/OstXtd
Assignment 5       | 102 out of 102  | https://goo.gl/BJitZx

\* Fixed the problems that caused point reduction, now should be 100 out of 100 in the code I'm publishing

(The semantic problems of making an inner method public instead of private and of using instanceof checks in places where it was unnecessary were fixed; I've also fixed the problem of going over all the matrix in assignment 3)