import java.util.Scanner;

public class Task8 {
	public static void main (String args[]){
        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
		Scanner myScanner=new Scanner(System.in);
		//Getting input from user
		int x1=myScanner.nextInt();
		int x2=myScanner.nextInt();
		int x3=myScanner.nextInt();
		int temp;
		//Putting the smallest number among (x1, x2) in x1
		if(x1>x2) {
			temp=x2;
			x2=x1;
			x1=temp;
		}
		//Right now we have: [minValue of first two, maxValue of first two, something]
		//Putting the smallest number among (x1, x3) in x1 - the minValue and the unknown number
		if(x1>x3) {
			temp=x3;
			x3=x1;x1=temp;
		}
		//Right now we have: [minValue of all, maxValue of first two, maxValue of minValue&unknown]
		//Putting the smallest number among (x2, x3) in x2 - comparing the "maxValue"s
		if(x2>x3) {
			temp=x3;
			x3=x2;
			x2=temp;
		}
		//Printing the sorted numbers
		System.out.println(x1);
		System.out.println(x2);
		System.out.println(x3);
        // ----------------- write your code ABOVE this line only ---------
	}
}
