import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
		Scanner myScanner=new Scanner(System.in);
		//Getting input from user - determines the number of trials we'll have
		int trialsNumber=myScanner.nextInt();
		int surface=0; //counter for the number of points that are inside the square
		int circle=0; //counter for the number of points that are inside the circle
		double x, y, hypotenuse;
		for(int i=0; i<trialsNumber; i++) {
			//Getting random x and y values for points - both between 0 and 1 -
			//that's because the square's edge and the circle's diameter is 2,
			//and in a quarter of that we get values between 0 and 1 in the first quadrant
			x=Math.random();
			y=Math.random();
			hypotenuse=Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2)); //calculating the distance between the random point and the origin
			//if that distance is less than the radios, the point is inside the (quarter of a) circle
			if(hypotenuse<1) {
				circle++;
			}
			//the point is inside the (quarter of a) square anyway
			surface++;
		}
		double pi=4.0*((double)circle/(double)surface); //using the Monte Carlo method
		System.out.println(pi);
        // ----------------- write your code ABOVE this line only ---------
	}
}
