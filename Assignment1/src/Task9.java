// You may not change or erase any of the lines and comments 
// in this file. You may only add lines in the designated 
// area.

import java.util.Scanner;

public class Task9 {


    public static void main(String[] args){
            Scanner scanner = new Scanner(System.in);

            int x1 = scanner.nextInt();
            int x2 = scanner.nextInt();
            int x3 = scanner.nextInt();	
            int x4 = scanner.nextInt();

            // ----------------- "A": write your code BELOW this line only --------
            // your code here (add lines)
            int temp;
            //Putting the smallest number among (x1,x2) in x1
            if(x1>x2) {
    			temp=x2;
    			x2=x1;
    			x1=temp;
    		}
            //Putting the smallest number among (x3,x4) in x3
            if(x3>x4) {
    			temp=x4;
    			x4=x3;
    			x3=temp;
    		}
            //Right now we have: [minValue of first two, maxValue of first two, minValue of last two, maxValue of last two]
            //Putting the smallest number among (x1,x3) in x1 - comparing the "minValue"
    		if(x1>x3) {
    			temp=x3;
    			x3=x1;
    			x1=temp;
    		}
    		//Right now we have: [minValue of all, maxValue of last two, maxValue of minValue, maxValue of last two]
            //Putting the smallest number among (x2,x4) in x2 - comparing the "maxValue" of the biggest (first&last two)
    		if(x2>x4) {
    			temp=x4;
    			x4=x2;
    			x2=temp;
    		}
    		//Right now we have: [minValue of all, minValue of maxValue, maxValue of minValue, maxValue of all]
            //Putting the smallest number among (x2,x3) in x2 - comparing the middle parts
    		if(x2>x3) {
    			temp=x3;
    			x3=x2;
    			x2=temp;
    		}
    		//Now we got the numbers sorted 
            // ----------------- "B" write your code ABOVE this line only ---------

            System.out.println(x1);
            System.out.println(x2);
            System.out.println(x3);
            System.out.println(x4);

    } // end of main
} //end of class Task9

