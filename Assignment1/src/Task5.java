public class Task5 {
	public static void main(String[] args) {
        final int m = Integer.MAX_VALUE;
        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
        int firstNumber=1;
		int secondNumber=1;
		int fib=0;
		boolean isExceed=false;
		//Calculating fibonacci, where the value in less than Integer.MAX_VALUE
		while(fib<m && !isExceed){
			firstNumber=secondNumber;
			secondNumber=fib;
			fib=firstNumber+secondNumber;
			//Making sure we won't exceed the MAX_VALUE
			//(if we do exceed, we'll get a negative number as a result of the addition of two positive numbers)
			if(firstNumber<0 || secondNumber<0 || fib<0) {
				isExceed=true;
			}
		}
		System.out.println(firstNumber);
		System.out.println(secondNumber);
        // ----------------- write your code ABOVE this line only ---------
	}
}
